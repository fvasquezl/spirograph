import math
from turtle import Turtle, Screen, colormode
from random import random, randint, choice


def random_color():
    r = randint(0,255)
    g = randint(0,255)
    b = randint(0,255)
    color = (r, g, b)
    return color


tim = Turtle()
colormode(255)
tim.speed('slowest')


def draw_spirograph(size_of_gap):
    for _ in range(int(360/size_of_gap)):
        tim.color(random_color())
        tim.circle(100)
        tim.setheading(tim.heading()+size_of_gap)


draw_spirograph(5)

screen = Screen()
screen.exitonclick()
